﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Lab_1
{
    [Serializable]
    class Student : Person, IDateAndCopy, INotifyPropertyChanged
    {
        private Education _education;
        private int _group;
        private List<Test> _tests;
        private List<Exam> _exams;

        public delegate void StudentsChangedHandler<TKey>(object source, StudentsChangedEventArgs<TKey> args);
        public event PropertyChangedEventHandler PropertyChanged;

        public DateTime Date { get; set; }

        public Education MyEducation
        {
            get { return _education; }
            set
            {
                _education = value;
                OnPropertyChanged("MyEducation");
            }
        }

        public List<Exam> Exams { get { return _exams; } set { _exams = value; } }

        public List<Test> Tests { get { return _tests; } set { _tests = value; } }

        public Student(Person myPersonValue, Education myEducationValue, int groupValue) : base(myPersonValue)
        {
            _education = myEducationValue;
            Group = groupValue;
            _exams = new List<Exam>();
            _tests = new List<Test>();
        }

        public Student() : this(new Person(), Education.Вachelor, 101) { }

        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool this[Education education]
        {
            get
            {
                return _education == education;
            }
        }

        public void AddExams(Exam[] examValue)
        {
            foreach (var item in examValue)
            {
                _exams.Add(item);
            }
        }
        
        public void AddExams(Exam examValue)
        {
            _exams.Add(examValue);
        }

        public void SortExamBySubject()
        {
            _exams?.Sort();
        }

        public void SortExamByGrade()
        {
            var exam = new Exam();
            _exams?.Sort(exam);
        }

        public void SortExamByData()
        {
            var e = new Exam.ExamComparer();
            _exams?.Sort(e);
        }

        public void AddTests(Test[] testsValue)
        {
            foreach (var item in testsValue)
            {
                _tests.Add(item);
            }
        }

        public override object DeepCopy()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using MemoryStream memoryStream = new MemoryStream();
            binaryFormatter.Serialize(memoryStream, this);
            memoryStream.Seek(0, SeekOrigin.Begin);
            Student retVal = (Student)binaryFormatter.Deserialize(memoryStream);
            memoryStream.Close();
            return retVal;
        }

        public bool Save(string filename)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            try
            {
                using Stream memoryStream = File.OpenWrite(filename);
                try
                {
                    binaryFormatter.Serialize(memoryStream, this);
                    memoryStream.Close();
                    return true;
                }
                catch (Exception)
                {
                    memoryStream.Close();
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Reset(Student backup)
        {
            MyPerson = backup.MyPerson;
            Group = backup.Group;
            MyEducation = backup.MyEducation;
            Exams = backup.Exams;
            Tests = backup.Tests;
        }

        public bool Load(string filename)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            Student oldStudent = (Student)DeepCopy();
            try
            {
                using Stream stream = File.OpenRead(filename);
                try
                {
                    Student newStudent = (Student)binaryFormatter.Deserialize(stream);
                    Reset(newStudent);
                    stream.Close();
                    return true;
                }
                catch (Exception)
                {
                    Reset(oldStudent);
                    stream.Close();
                    return false;
                }
            }
            catch (Exception)
            {
                Reset(oldStudent);
                return false;
            }
        }

        public static bool Save(string filename, ref Student studentToSave)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            try
            {
                using Stream stream = File.OpenWrite(filename);
                try
                {
                    binaryFormatter.Serialize(stream, studentToSave);
                    stream.Close();
                    return true;
                }
                catch (Exception)
                {
                    stream.Close();
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool Load(string filename, ref Student student)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Student backup = (Student)student.DeepCopy();
            try
            {
                using Stream stream = File.OpenRead(filename);
                try
                {
                    Student loadStudent = (Student)formatter.Deserialize(stream);
                    student.Reset(loadStudent);
                    stream.Close();
                    return true;
                }
                catch (Exception)
                {
                    student = backup;
                    stream.Close();
                    return false;
                }
            }
            catch (Exception)
            {
                student = backup;
                return false;
            }
        }

        public bool AddFromConsole()
        {
            Console.WriteLine("Ввод данных об экзаме. ");
            Console.WriteLine("Введите данные в формате: предмет;оценка;день(дд);месяц(мм);год(гггг)");
            string input = Console.ReadLine();
            string[] data = input.Split(";");
            string subject;
            int grade;
            int day;
            int month;
            int year;
            DateTime date;
            try
            {
                subject = data[0];
                grade = int.Parse(data[1]);
                day = int.Parse(data[2]);
                month = int.Parse(data[3]);
                year = int.Parse(data[4]);
                date = new DateTime(year, month, day);
            }
            catch (Exception)
            {
                return false;
            }

            Exam newExam = new Exam(subject, grade, date);
            AddExams(newExam);
            return true;
        }

        public override string ToString()
        {
            string result = base.ToString() + " " + _education.ToString() + " " + Convert.ToString(_group);
            foreach (Exam exam in _exams)
            {
                result += " " + exam.Subject;
            }
            foreach (Test test in _tests)
            {
                result += " " + test.Subject;
            }
            return result;
        }

        public override string ToShortString()
        {
            return MyPerson.ToString() + " " + _education.ToString() + " " + Convert.ToString(_group) + " " + Convert.ToString(AverageExamGrade);
        }

        public void PrintExams()
        {
            foreach (var item in _exams)
            {
                Console.WriteLine(item.ToString());
            }
        }

        public double AverageExamGrade
        {
            get
            {
                double result = 0;
                if (_exams.Count != 0)
                {
                    foreach (Exam exam in _exams)
                    {
                        result += exam.Grade;
                    }
                    return result / _exams.Count;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int QuantityExams
        {
            get
            {
                return _exams.Count;
            }
        }

        public int QuantityTests
        {
            get
            {
                return _tests.Count;
            }
        }

        public Education GetEducation()
        {
            return _education;
        }

        public Person MyPerson
        {
            get
            {
                return new Person(_name, _surname, _birthday);
            }
            set
            {
                _name = value.Name;
                _surname = value.Surname;
                _birthday = value.Birthday;
            }
        }

        public int Group
        {
            get
            {
                return _group;
            }
            set
            {
                if (value < 101 | value > 599)
                {
                    throw new IndexOutOfRangeException("Значение поля _group должно быть от 101 до 599 включительно.");
                }
                else
                {
                    _group = value;
                    OnPropertyChanged("Group");
                }
            }
        }

        public IEnumerable GetExamsAndTests()
        {
            for (int i = 0; i < _exams.Count; i++)
            {
                yield return _exams[i];
            }

            for (int i = 0; i < _tests.Count; i++)
            {
                yield return _tests[i];
            }
        }

        public IEnumerable GetExams(int minimalGrade)
        {
            for (int i = 0; i < _exams.Count; i++)
            {
                if ((_exams[i] as Exam).Grade >= minimalGrade)
                {
                    yield return _exams[i];
                }
            }
        }

        public IEnumerable GetPassedTest()
        {
            for (int i = 0; i < _tests.Count; i++)
            {
                if ((_tests[i] as Test).IsTestPassed)
                {
                    yield return _tests[i];
                }
            }
        }

        public IEnumerable GetPassedExamsAndTest()
        {
            foreach (Exam exam in this.GetExams(3))
            {
                yield return exam;
            }

            foreach (Test test in this.GetPassedTest())
            {
                yield return test;
            }
        }

        public IEnumerable GetPassedTestsWithPassedExams()
        {
            List<string> examSubjects = new List<string>();
            foreach (Exam exam in this.GetExams(3))
            {
                examSubjects.Add(exam.Subject);
            }

            foreach (Test test in this.GetPassedTest())
            {
                foreach (string subject in examSubjects)
                {
                    if (String.Compare(test.Subject, subject) == 0)
                    {
                        yield return test;
                    }
                }
            }

        }

        public IEnumerator GetEnumerator()
        {
            return new StudentEnumerator(_exams, _tests);
        }

        class StudentEnumerator : IEnumerator
        {
            List<Exam> _exams;
            List<Test> _tests;
            int _position;

            public StudentEnumerator(List<Exam> exams, List<Test> tests)
            {
                _exams = exams;
                _tests = tests;
                _position = -1;
            }

            public object Current
            {
                get
                {
                    if (_position < 0 || _position >= _exams.Count) throw new InvalidOperationException();
                    return (_exams[_position] as Exam).Subject;
                }
            }

            public bool MoveNext()
            {
                Exam exam;
                Test test;
                _position++;

                while (_position < _exams.Count)
                {
                    exam = _exams[_position] as Exam;
                    for (int i = 0; i < _tests.Count; i++)
                    {
                        test = _tests[i] as Test;
                        if (String.Compare(exam.Subject, test.Subject) == 0)
                        {
                            return true;
                        }
                    }
                    _position++;
                }
                return false;
            }

            public void Reset()
            {
                _position = -1;
            }
        }

        public class StudentComparer : IComparer<Student>
        {
            public int Compare(Student x, Student y)
            {
                double deltaAverageGrade = x.AverageExamGrade - y.AverageExamGrade;
                if (deltaAverageGrade == 0)
                {
                    return 0;
                }
                else
                {
                    return (int)((deltaAverageGrade) / Math.Abs(deltaAverageGrade));
                }
            }
        }
    }
}
