﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_1
{
    [Serializable]
    class Test
    {
        public string Subject { get; set; }
        public bool IsTestPassed { get; set; }

        public Test(string subjectValue, bool isTestPassedValue)
        {
            Subject = subjectValue;
            IsTestPassed = isTestPassedValue;
        }

        public Test() : this("test", false) { }

        public override string ToString()
        {
            return Subject + " " + IsTestPassed.ToString();
        }
    }
}
