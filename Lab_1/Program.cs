﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lab_1
{
    delegate TKey KeySelector<TKey>(Student st);

    class Program
    {

        static void Main(string[] args)
        {
            Lab5();
        }

        public static void Lab5()
        {
            // 1)
            Console.WriteLine("1)");
            Person p1 = new Person("name1", "sur1", new DateTime(2001, 1, 4));
            Student student1 = new Student(p1, Education.Вachelor, 101);
            student1.AddExams(new Exam("exam_1", 5, new DateTime(2021, 9, 1)));

            Student student2 = (Student)student1.DeepCopy();

            Console.WriteLine("Оригинал: " + student1);
            Console.WriteLine("Копия: " + student2);


            // 2)
            Console.WriteLine("\n2)");
            Console.WriteLine("Введите название файла:\nНапример: file_name");

            string fname = Console.ReadLine();
            if (!File.Exists(fname))
            {
                try
                {
                    File.Create(fname);
                    Console.WriteLine("Такого файла нет.\nСоздан файл: " + fname);
                }
                catch (IOException)
                {
                    Console.WriteLine("Некорректное имя файла.");
                    fname = "file_name.bin";
                    if (!File.Exists(fname))
                    {
                        File.Create(fname);
                        Console.WriteLine("file not detected; corresponding file created");
                    }
                    else
                    {
                        student2.Load(fname);
                    }
                }
            }
            else
            {
                if (!student2.Load(fname))
                {
                    Console.WriteLine("Загрузка не удалась.");
                }
                else
                {
                    Console.WriteLine("Загрузка успешна.");
                }
            }
            

            // 3)
            Console.WriteLine("\n3)");
            Console.WriteLine("Загружено: " + student2);

            // 4)
            Console.WriteLine("\n4)");
            bool success = false;
            if (!success)
            {
                success = student2.AddFromConsole();
                if (!success)
                {
                    Console.WriteLine("Некорректный ввод");
                }
            }
            student2.Save(fname);
            Console.WriteLine(student2);


            // 5)
            Console.WriteLine("\n5)");
            Student.Load(fname, ref student2);
            success = false;
            if (!success)
            {
                success = student2.AddFromConsole();
                if (!success)
                {
                    Console.WriteLine("Некорректный ввод");
                }
            }
            Student.Save(fname, ref student2);
            Console.WriteLine(student2);
        }

        public static void Lab4()
        {
            KeySelector<string> selector = delegate (Student input)
            {
                return input.GetHashCode().ToString();
            };

            Person p1 = new Person("aaa", "zzz", new DateTime(1, 1, 1));
            Person p2 = new Person("sss", "xxx", new DateTime(2, 2, 2));
            Person p3 = new Person("ddd", "ccc", new DateTime(3, 3, 3));
            Person p4 = new Person("fff", "vvv", new DateTime(4, 4, 4));

            Student student1 = new Student(p1, new Education(), 110);
            Student student2 = new Student(p2, new Education(), 120);
            Student student3 = new Student(p3, new Education(), 130);
            Student student4 = new Student(p4, new Education(), 140);

            var colection_1 = new StudentCollection<string>(selector);
            colection_1.CollectionName = "Collection_1";
            var collection_2 = new StudentCollection<string>(selector);
            collection_2.CollectionName = "Collection_2";

            Journal journal = new Journal();
            colection_1.StudentsChanged += journal.NewListEntry;
            collection_2.StudentsChanged += journal.NewListEntry;

            student1.PropertyChanged += journal.NewListEntry;
            student2.PropertyChanged += journal.NewListEntry;
            student3.PropertyChanged += journal.NewListEntry;
            student4.PropertyChanged += journal.NewListEntry;

            Student[] students1 = new Student[2];
            students1[0] = student1;
            students1[1] = student2;
            Student[] students2 = new Student[2];
            students2[0] = student3;
            students2[1] = student4;

            colection_1.AddStudents(students1);
            collection_2.AddStudents(students2);

            student3.MyEducation = Education.Specialist;
            student2.Group = 150;

            colection_1.Remove(student2);
            student2.MyEducation = Education.Specialist;
            student2.Group = 150;

            Console.WriteLine(journal.ToString());
        }

        public static void Lab3()
        {
            //1
            Console.WriteLine("1)");
            Person p1 = new Person("name1", "sur1", new DateTime(2001, 1, 4));
            Student student = new Student(p1, Education.Вachelor, 101);
            Exam[] exams = {
                new Exam("exam_3", 3, new DateTime(2021, 9, 1)),
                new Exam("exam_2", 4, new DateTime(2021, 11, 1)),
                new Exam("exam_1", 5, new DateTime(2021, 10, 1))
            };

            student.AddExams(exams);

            Console.WriteLine("Сортироввка по предмету");
            student.SortExamBySubject();
            student.PrintExams();

            Console.WriteLine("\nСортироввка по оцеке");
            student.SortExamByGrade();
            student.PrintExams();

            Console.WriteLine("\nСортироввка по дате");
            student.SortExamByData();
            student.PrintExams();

            //2
            Console.WriteLine("\n2)");
            Person p2 = new Person("name2", "sur2", new DateTime(2002, 4, 25));
            Person p3 = new Person("name3", "sur3", new DateTime(2003, 2, 4));

            StudentCollection<string> studentCollection = new StudentCollection<string>(KeyGenerator1);
            Student[] students =
            {
                new Student(p1, Education.Вachelor, 200),
                new Student(p2, Education.Specialist, 300),
                new Student(p3, Education.Вachelor, 400)
            };
            studentCollection.AddStudents(students);
            Console.WriteLine(studentCollection.ToString());

            //3
            Console.WriteLine("\n3)");

            Exam[] exams2 = {
                new Exam("exam_3", 5, new DateTime(2021, 9, 1)),
                new Exam("exam_2", 5, new DateTime(2021, 11, 1)),
                new Exam("exam_1", 4, new DateTime(2021, 10, 1))
            };

            Exam[] exams3 = {
                new Exam("exam_3", 4, new DateTime(2021, 9, 1)),
                new Exam("exam_2", 5, new DateTime(2021, 11, 1)),
                new Exam("exam_1", 4, new DateTime(2021, 10, 1))
            };

            students[0].AddExams(exams);
            students[1].AddExams(exams2);
            students[2].AddExams(exams3);

            Console.WriteLine("Наибольший средний балл: {0:f}", studentCollection.MaxExamGrade);

            Console.WriteLine("\nСтуденты бакалавры:");
            foreach (var item in studentCollection.EducationForm(Education.Вachelor))
            {
                Console.WriteLine(item.ToString());
            }


            Console.WriteLine("\nГруппы студентов по образованию:");
            foreach (var item in studentCollection.GroupBuEducation)
            {
                Console.WriteLine(item.Key.ToString());
                foreach (var subItem in item)
                {
                    Console.WriteLine(subItem.Value.ToString());
                }
                Console.WriteLine();
            }

            //4
            Console.WriteLine("\n4)");
            Console.WriteLine("Введите размер коллекции:");
            int lengthCollection = 0;
            while(lengthCollection == 0)
            {
                try
                {
                    lengthCollection = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    Console.WriteLine("Нужно ввести число");
                }
            }


            TestCollection<Person, Student> testCollection = new TestCollection<Person, Student>(lengthCollection, ElementGenerator);

            testCollection.ScanKeyList();
            testCollection.ScanStringDictionaryByKey();
            testCollection.ScanStringList();
            testCollection.ScanTKeyDictionaryByKey();
            testCollection.ScanTKeyDictionaryByValue();


        }

        public static string KeyGenerator1(Student student)
        {
            return student.GetHashCode().ToString();
        }

        public static string KeyGenerator2(Person person)
        {
            return person.GetHashCode().ToString();
        }

        public static KeyValuePair<Person, Student> ElementGenerator(int elementGenerationCode)
        {
            int str = elementGenerationCode + 1;
            for (int i = 0; i < 5; i++)
            {
                str = (str * i) + (str / (i + 1));
            }
            string name = str.ToString();
            Person p = new Person(name, name, new DateTime(1, 1, 1));
            Student s = new Student(p, Education.Specialist, 101);
            return new KeyValuePair<Person, Student>(p, s);
        }

    public static void Lab2()
    {
        //1
        Console.WriteLine("1)");

        Person p1 = new Person("aaa", "fff", new DateTime(1, 1, 1));
        Person p2 = new Person("aaa", "fff", new DateTime(1, 1, 1));

        Console.WriteLine(p1 == p2);
        Console.WriteLine("Ссылки совпадают: {0}", ReferenceEquals(p1, p2));
        Console.WriteLine(p1.GetHashCode());
        Console.WriteLine(p2.GetHashCode());

        //2
        Console.WriteLine("\n2)");
        Student student = new Student(p1, Education.Вachelor, 101);

        Test[] tests = new Test[1];
        tests[0] = new Test("test_1", true);
        student.AddTests(tests);

        Exam[] exams = new Exam[1];
        exams[0] = new Exam("exam_1", 3, new DateTime(1, 1, 1));
        student.AddExams(exams);

        Console.WriteLine(student.ToString());

        //3
        Console.WriteLine("\n3)");
        Console.WriteLine(student.MyPerson.ToString());

        //4
        Console.WriteLine("\n4)");

        Student studentCopy = student.DeepCopy() as Student;
        student.Name = "rrr";

        Console.WriteLine("Оригиал: {0}", student.ToShortString());
        Console.WriteLine("Копия:   {0}", studentCopy.ToShortString());

        //5
        Console.WriteLine("\n5)");

        try
        {
            student.Group = 100;
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception.Message);
        }

        //6
        Console.WriteLine("\n6)");

        foreach (var item in student.GetExamsAndTests())
        {
            Console.WriteLine(item);
        }

        //7
        Console.WriteLine("\n7)");

        exams = new Exam[2];
        exams[0] = new Exam("exam_2", 4, new DateTime(1, 1, 1));
        exams[1] = new Exam("exam_3", 5, new DateTime(1, 1, 1));
        student.AddExams(exams);

        foreach (var item in student.GetExams(4))
        {
            Console.WriteLine(item);
        }

        //8
        Console.WriteLine("\n8)");

        exams = new Exam[2];
        exams[0] = new Exam("physics", 4, new DateTime(1, 1, 1));
        exams[1] = new Exam("math", 5, new DateTime(1, 1, 1));
        student.AddExams(exams);

        tests = new Test[2];
        tests[1] = new Test("physics", true);
        tests[0] = new Test("math", false);
        student.AddTests(tests);

        foreach (string subject in student)
        {
            Console.WriteLine(subject);
        }

        //9
        Console.WriteLine("\n9)");

        foreach (var item in student.GetPassedExamsAndTest())
        {
            Console.WriteLine(item.ToString());
        }

        //10
        Console.WriteLine("\n10)");

        foreach (Test test in student.GetPassedTestsWithPassedExams())
        {
            Console.WriteLine(test.ToString());
        }

    }

}

}
