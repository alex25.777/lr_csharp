﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_1
{
    class StudentsChangedEventArgs<TKey> : EventArgs
    {
        public string CollectionName { get; set; }
        public Action EventType { get; set; }
        public string PropertyName { get; set; }
        public TKey ElementKey { get; set; }
        public StudentsChangedEventArgs(string Name, Action Kind, string Prop, TKey Key)
        {
            this.CollectionName = Name;
            this.EventType = Kind;
            this.PropertyName = Prop;
            this.ElementKey = Key;
        }
        public override string ToString()
        {
            return this.CollectionName + "\n" + this.EventType.ToString() + "\n" + this.PropertyName + "\n" + this.ElementKey.ToString() + "\n" + "\n";
        }
    }

}