﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Lab_1
{
    class Journal
    {
        private List<JournalEntry> ListOfAllEntry = new List<JournalEntry>();

        public void NewListEntry(object subject, EventArgs e)
        {
            if (e is StudentsChangedEventArgs<string>)
            {
                var currentEvant = e as StudentsChangedEventArgs<string>;
                JournalEntry newListEntry = new JournalEntry(currentEvant.CollectionName, currentEvant.EventType, currentEvant.PropertyName, currentEvant.ElementKey.ToString());
                ListOfAllEntry.Add(newListEntry);
                return;
            }

            if (e is PropertyChangedEventArgs)
            {
                PropertyChangedEventArgs currentEvant = e as PropertyChangedEventArgs;
                JournalEntry newListEntry = new JournalEntry("", Action.Property, currentEvant.PropertyName.ToString(), "");
                ListOfAllEntry.Add(newListEntry);
                return;
            }

            throw new ArgumentException();
        }
        

        public override string ToString()
        {
            string entryNames = "";
            foreach (JournalEntry item in ListOfAllEntry)
            {
                entryNames += item.ToString() + "\n";
            }
            return entryNames;
        }
    }
}
