﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Lab_1
{
    [Serializable]
    class Exam : IDateAndCopy, IComparable, IComparer<Exam>
    {
        private string _subject;
        private int _grade;
        private DateTime _examDate;

        public DateTime Date { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Exam(string subjectValue, int gradeValue, DateTime examDateValue)
        {
            _subject = subjectValue;
            _grade = gradeValue;
            _examDate = examDateValue;
        }

        public Exam() : this("subject", 0, new DateTime()) { }

        public Object DeepCopy()
        {
            return new Exam(_subject, _grade, _examDate);
        }

        public override string ToString()
        {
            return _subject + " " + Convert.ToString(_grade) + " " + _examDate.ToShortDateString();
        }

        public int CompareTo(object obj)
        {
            Exam exam;
            if(obj is Exam)
            {
                exam = obj as Exam;
            }
            else
            {
                return -1;
            }
            return String.Compare(_subject, exam._subject);
        }

        public int Compare([AllowNull] Exam x, [AllowNull] Exam y)
        {
            double deltaGgrade = x._grade - y._grade;
            if (deltaGgrade == 0)
            {
                return 0;
            }
            else
            {
                return (int)((deltaGgrade) / Math.Abs(deltaGgrade));
            }
        }

        public int Grade { get { return _grade; } set { _grade = value; } }

        public string Subject { get { return _subject; } set { _subject = value; } }

        public class ExamComparer : IComparer<Exam>
        {
            public int Compare([AllowNull] Exam x, [AllowNull] Exam y)
            {
                return DateTime.Compare(x._examDate, y._examDate);
            }
        }
    }
}
