﻿using System;
using System.Collections.Generic;
using System.Linq;
using static Lab_1.Student;
using System.ComponentModel;

namespace Lab_1
{
    class StudentCollection<TKey>
    {
        private Dictionary<TKey, Student> _students;
        private KeySelector<TKey> _keyGenerator;

        public string CollectionName { get; set; }

        public event StudentsChangedHandler<TKey> StudentsChanged;

        private void StudentPropertyChanged(Action act, string name, TKey key)
        {
            if (StudentsChanged != null)
            {
                StudentsChanged(this, new StudentsChangedEventArgs<TKey>(CollectionName, act, name, key));
            }
        }

        public StudentCollection(KeySelector<TKey> keyGenerator)
        {
            _students = new Dictionary<TKey, Student>();
            _keyGenerator = keyGenerator;
        }

        public void TranslateEvent(object studentValue, EventArgs e)
        {
            var currentEvent = e as PropertyChangedEventArgs;
            var student = studentValue as Student;
            TKey key = _keyGenerator(student);
            StudentPropertyChanged(Action.Property, currentEvent.PropertyName.ToString(), key);
        }

        public void AddDefaults()
        {
            Student newStudent = new Student();
            TKey key = _keyGenerator(newStudent);
            _students.Add(key, newStudent);
            StudentPropertyChanged(Action.Add, "collection", key);

            newStudent.PropertyChanged += TranslateEvent;
        }

        public void AddDefaults(int quantity)
        {
            Student newStudent;
            for (int i = 0; i < quantity; i++)
            {
                newStudent = new Student();
                TKey key = _keyGenerator(newStudent);
                _students.Add(_keyGenerator(newStudent), newStudent);
                StudentPropertyChanged(Action.Add, "collection", key);
                newStudent.PropertyChanged += TranslateEvent;
            }
        }

        public void AddStudents(Student[] studentsValue)
        {
            if (studentsValue != null)
            {
                TKey key;
                foreach (Student student in studentsValue)
                {
                    key = _keyGenerator(student);
                    _students.Add(_keyGenerator(student), student);
                    StudentPropertyChanged(Action.Add, "collection", key);
                    student.PropertyChanged += TranslateEvent;
                }
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public bool Remove(Student student)
        {
            TKey key = _keyGenerator(student);
            if (_students.ContainsKey(key))
            {
                _students.Remove(key);
                StudentPropertyChanged(Action.Remove, "collection", key);
                student.PropertyChanged -= TranslateEvent;
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            string result = "";
            Student student;
            if (_students != null)
            {
                foreach (var st in _students)
                {
                    student = st.Value;
                    result += st.Key.ToString() + " " + student.ToString() + '\n';
                }
            }
            return result;
        }

        public string ToShortString()
        {
            string result = "";
            Student student;
            if (_students != null)
            {
                foreach (var st in _students)
                {
                    student = st.Value;
                    result += student.ToShortString() + " " + student.QuantityExams.ToString() + " " + student.QuantityTests.ToString() + '\n';
                }
            }
            return result;
        }

        public IEnumerable<KeyValuePair<TKey, Student>> EducationForm(Education value)
        {
            return _students.Where(student => student.Value.GetEducation() == value);
        }

        public IEnumerable<IGrouping<Education, KeyValuePair<TKey, Student>>> GroupBuEducation
        {
            get
            {
                return _students.GroupBy(student => student.Value.GetEducation());
            }
        }


        public double MaxExamGrade
        {
            get
            {
                if (_students.Count == 0) return 0;
                return _students.Max(kvp => kvp.Value.AverageExamGrade);
            }
        }
    }
}
