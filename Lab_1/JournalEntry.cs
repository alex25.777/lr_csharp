﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_1
{
    class JournalEntry
    {
        private string _collectionName;
        private Action _eventType;
        private string _studentProperty;
        private string _key;

        public JournalEntry(string collectionName, Action eventType, string studentProperty, string key)
        {
            _collectionName = collectionName;
            _eventType = eventType;
            _studentProperty = studentProperty;
            _key = key;
        }

        public override string ToString()
        {
            return _collectionName + " " + _eventType.ToString() + " " + _studentProperty + " " + _key;
        }
    }
}
