﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Lab_1
{
    public delegate KeyValuePair<TKey, TValue> GenerateElement<TKey, TValue>(int elementGenerationCode);

    public class TestCollection<TKey, TValue>
    {
        private List<TKey> _keyList = new List<TKey>();
        private List<string> _stringList = new List<string>();
        private Dictionary<TKey, TValue> _KeyDictionary = new Dictionary<TKey, TValue>();
        private Dictionary<string, TValue> _stringDictionary = new Dictionary<string, TValue>();
        private GenerateElement<TKey, TValue> _generateElement;

        public TestCollection(int count, GenerateElement<TKey, TValue> generateElement)
        {
            _generateElement = generateElement;
            for (int i = 0; i < count; i++)
            {
                KeyValuePair<TKey, TValue> element = _generateElement(i);
                _KeyDictionary.Add(element.Key, element.Value);
                _stringDictionary.Add(element.Key.ToString(), element.Value);
                _keyList.Add(element.Key);
                _stringList.Add(element.Key.ToString());
            }
        }

        public void ScanKeyList()
        {
            Console.WriteLine("\nПоиск в KeyList:");

            TKey first = _keyList[0];
            TKey middle = _keyList[_keyList.Count / 2];
            TKey last = _keyList[_keyList.Count - 1];
            TKey none = _generateElement(_keyList.Count + 1).Key;

            Stopwatch watch = Stopwatch.StartNew();
            _keyList.Contains(first);
            watch.Stop();
            Console.WriteLine("Для первого элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _keyList.Contains(middle);
            watch.Stop();
            Console.WriteLine("Для центрального элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _keyList.Contains(last);
            watch.Stop();
            Console.WriteLine("Для последнего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _keyList.Contains(none);
            watch.Stop();
            Console.WriteLine("Для отсутствующего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");
        }

        public void ScanStringList()
        {
            Console.WriteLine("\nПоиск в StringList:");

            string first = _stringList[0];
            string middle = _stringList[_stringList.Count / 2];
            string last = _stringList[_stringList.Count - 1];
            string none = _generateElement(_stringList.Count + 1).Key.ToString();

            Stopwatch watch = Stopwatch.StartNew();
            _stringList.Contains(first);
            watch.Stop();
            Console.WriteLine("Для первого элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _stringList.Contains(middle);
            watch.Stop();
            Console.WriteLine("Для центрального элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _stringList.Contains(last);
            watch.Stop();
            Console.WriteLine("Для последнего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _stringList.Contains(none);
            watch.Stop();
            Console.WriteLine("Для отсутствующего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");
        }

        public void ScanTKeyDictionaryByKey()
        {
            Console.WriteLine("\nПоиск в TKeyDictionary по ключу:");

            TKey first = _KeyDictionary.ElementAt(0).Key;
            TKey middle = _KeyDictionary.ElementAt(_KeyDictionary.Count / 2).Key;
            TKey last = _KeyDictionary.ElementAt(_KeyDictionary.Count - 1).Key;
            TKey none = _generateElement(_KeyDictionary.Count + 1).Key;

            Stopwatch watch = Stopwatch.StartNew();
            _KeyDictionary.ContainsKey(first);
            watch.Stop();
            Console.WriteLine("Для первого элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _KeyDictionary.ContainsKey(middle);
            watch.Stop();
            Console.WriteLine("Для центрального элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _KeyDictionary.ContainsKey(last);
            watch.Stop();
            Console.WriteLine("Для последнего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _KeyDictionary.ContainsKey(none);
            watch.Stop();
            Console.WriteLine("Для отсутствующего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");
        }

        public void ScanStringDictionaryByKey()
        {
            Console.WriteLine("\nПоиск в StringDictionary по ключу:");

            string first = _stringDictionary.ElementAt(0).Key;
            string middle = _stringDictionary.ElementAt(_stringDictionary.Count / 2).Key;
            string last = _stringDictionary.ElementAt(_stringDictionary.Count - 1).Key;
            string none = _generateElement(_stringDictionary.Count + 1).Key.ToString();

            Stopwatch watch = Stopwatch.StartNew();
            _stringDictionary.ContainsKey(first);
            watch.Stop();
            Console.WriteLine("Для первого элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _stringDictionary.ContainsKey(middle);
            watch.Stop();
            Console.WriteLine("Для центрального элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _stringDictionary.ContainsKey(last);
            watch.Stop();
            Console.WriteLine("Для последнего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _stringDictionary.ContainsKey(none);
            watch.Stop();
            Console.WriteLine("Для отсутствующего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");
        }

        public void ScanTKeyDictionaryByValue()
        {
            Console.WriteLine("\nПоиск в TKeyDictionary по значению:");

            TValue first = _KeyDictionary.ElementAt(0).Value;
            TValue middle = _KeyDictionary.ElementAt(_KeyDictionary.Count / 2).Value;
            TValue last = _KeyDictionary.ElementAt(_KeyDictionary.Count - 1).Value;
            TValue none = _generateElement(_KeyDictionary.Count + 1).Value;

            Stopwatch watch = Stopwatch.StartNew();
            _KeyDictionary.ContainsValue(first);
            watch.Stop();
            Console.WriteLine("Для первого элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _KeyDictionary.ContainsValue(middle);
            watch.Stop();
            Console.WriteLine("Для центрального элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _KeyDictionary.ContainsValue(last);
            watch.Stop();
            Console.WriteLine("Для последнего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");

            watch.Restart();
            _KeyDictionary.ContainsValue(none);
            watch.Stop();
            Console.WriteLine("Для отсутствующего элемента: " + watch.Elapsed.TotalMilliseconds + " мс");
        }
    }
}