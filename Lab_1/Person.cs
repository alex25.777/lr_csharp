﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Lab_1
{
    [Serializable]
    class Person : IDateAndCopy, IComparable, IComparer<Person>
    {
        protected string _name;
        protected string _surname;
        protected DateTime _birthday;
        public DateTime Date { get; set; }

        public Person(string nameValue, string surnameValue, DateTime birthdayValue)
        {
            _name = nameValue;
            _surname = surnameValue;
            _birthday = birthdayValue;
        }

        public Person() : this("name", "surname", new DateTime()) { }

        public Person(Person person) : this(person._name, person._surname, person._birthday) { }

        public override bool Equals(object obj)
        {
            Person person = null;
            if (obj is Person)
            {
                person = obj as Person;
            }
            else
            {
                return false;
            }

            bool result = true;
            result = result && (_name == person._name);
            result = result && (_surname == person._surname);
            result = result && (_birthday == person._birthday);
            return result;
        }

        public override int GetHashCode()
        {
            return _name.GetHashCode() + _surname.GetHashCode() + _birthday.GetHashCode();
        }

        public static bool operator ==(Person p1, Person p2)
        {
            return p2.Equals(p1);
        }

        public static bool operator !=(Person p1, Person p2)
        {
            return !p2.Equals(p1);
        }

        public virtual Object DeepCopy()
        {
            return new Person(_name, _surname, _birthday);
        }

        public override string ToString()
        {
            return _name + " " + _surname + " " + _birthday.ToShortDateString();
        }

        public virtual string ToShortString()
        {
            return _name + " " + _surname;
        }

        public int CompareTo(object obj)
        {
            Person other;
            if(obj is Person)
            {
                other = obj as Person;
            }
            else
            {
                return -1;
            }

            return String.Compare(Surname, other.Surname);
        }

        public int Compare(Person x, Person y)
        {
            return DateTime.Compare(x.Birthday, y.Birthday);
        }

        public string Name { get { return _name; } set { _name = value; } }
        public string Surname { get { return _surname; } set { _surname = value; } }
        public DateTime Birthday { get { return _birthday; } set { _birthday = value; } }

        internal Person Copy()
        {
            Person copy = new Person();
            copy.Name = Name;
            copy.Surname = Surname;
            copy.Birthday = Birthday;
            return copy;
        }
    }
}

